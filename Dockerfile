FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

WORKDIR /src

COPY AspNetCoreWebApi6/AspNetCoreWebApi6.csproj .

RUN dotnet restore "AspNetCoreWebApi6.csproj"

COPY AspNetCoreWebApi6 .

RUN dotnet build "AspNetCoreWebApi6.csproj" -c Release -o /app/build

RUN dotnet publish "AspNetCoreWebApi6.csproj" -c Release -o /app/publish



FROM mcr.microsoft.com/dotnet/aspnet:6.0

WORKDIR /app

COPY --from=build /app/publish .

ENTRYPOINT ["dotnet", "AspNetCoreWebApi6.dll"]
